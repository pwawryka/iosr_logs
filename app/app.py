#!/usr/bin/python
# -*- coding: utf-8 -*-

from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto.utils
import threading
import os
from flask import Flask
from flask import send_file
from flask import request
from io import BytesIO, StringIO
from PIL import Image, ImageFilter
import requests
import time
import shutil
import zipfile

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

conn = S3Connection()
bucket = conn.get_bucket('gridylab2')
data = boto.utils.get_instance_identity()
region_name = data['document']['region']
app = Flask(__name__)


def send_file_s3(bucket, filename):
    key = Key(bucket, filename)
    key.set_contents_from_filename(filename)
    key.make_public()
#os.remove(filename)

def process_image(url, timestamp, save):
    if not os.path.exists(timestamp) and save == 1:
        os.makedirs(timestamp)
    
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))

    bw_image_file = img.convert('1') # convert image to black and white
    blur_image_file = img.filter(ImageFilter.BLUR)
    min_image_file = img.filter(ImageFilter.MinFilter(3))
    edge_image_file = img.filter(ImageFilter.EDGE_ENHANCE_MORE)
    smooth_image_file = img.filter(ImageFilter.SMOOTH_MORE)
    find_edges_image_file = img.filter(ImageFilter.FIND_EDGES)
    contour_image_file = img.filter(ImageFilter.CONTOUR)
    detail_image_file = img.filter(ImageFilter.DETAIL)
    
    images = [bw_image_file,blur_image_file,min_image_file,edge_image_file,smooth_image_file,find_edges_image_file,contour_image_file,detail_image_file]
    
    if not save == 1:
        return
    
    for x in range(0, len(images)):
        img = images[x]
        img.save(timestamp + "/" + str(x) + ".jpg")
    
    shutil.make_archive(timestamp, 'zip', timestamp)
    send_file_s3(bucket, timestamp + ".zip")

@app.route("/")
def hello():
    return "Hello World!"


@app.route('/process_image')
def get_image():
    save = int(request.args.get("save"))
    url = request.args.get("url")
    timestamp = str(int(time.time()*10000))
    threading.Thread(target=process_image, args=(url, timestamp, save)).start()
    if save == 1:
        return "https://s3-%s.amazonaws.com/%s/%s" % (region_name, 'gridylab2', timestamp + ".zip")
    else:
        return "Faking address"

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=9898)
