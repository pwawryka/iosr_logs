FROM python:2.7

RUN apt-get update
RUN apt-get -y install libpcap0.8
RUN curl -L -O https://artifacts.elastic.co/downloads/beats/packetbeat/packetbeat-5.1.2-amd64.deb
RUN dpkg -i packetbeat-5.1.2-amd64.deb
ADD proper_packetbeat.yml /etc/packetbeat/packetbeat.yml

ADD app /app
WORKDIR /app
RUN pip install -r requirements.txt

CMD ["sh", "./start.sh"]

EXPOSE 9898
